package login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestCase_3 
{
	ChromeDriver driver;
	String BaseURL = "https://the-internet.herokuapp.com/login";
	@BeforeTest
	public void openURL() 
	{
		String value = System.getProperty("user.dir")+"\\\\Resources\\\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", value);
		driver = new ChromeDriver();
		driver.navigate().to(BaseURL);
	}
	
	@Test
	public void Wrong_Password()
	{
		WebElement User_Name = driver.findElement(By.id("username"));
		User_Name.sendKeys("tomsmith");
		
		WebElement PassWord = driver.findElement(By.id("password"));
		PassWord.sendKeys("Password!");
		
		WebElement Button = driver.findElement(By.className("radius"));
		Button.click();

   }
}