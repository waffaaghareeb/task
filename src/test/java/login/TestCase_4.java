package login;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestCase_4 
{
	ChromeDriver driver;
	String BaseURL = "https://the-internet.herokuapp.com/login";
	@BeforeTest
	public void openURL() 
	{
		String value = System.getProperty("user.dir")+"\\\\Resources\\\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", value);
		driver = new ChromeDriver();
		driver.navigate().to(BaseURL);
	}
	//empty username and PassWord
	@Test
	public void Empty_data()
	{
		WebElement User_Name = driver.findElement(By.id("username"));
		User_Name.sendKeys("");
		
		WebElement PassWord = driver.findElement(By.id("password"));
		PassWord.sendKeys("");
		
		WebElement Button = driver.findElement(By.className("radius"));
		Button.click();

   }

}
